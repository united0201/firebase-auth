// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyAIroyXZ-uRhjheMm2gKlmc09sDuLTcrRo',
        authDomain: 'node-test-264012.firebaseapp.com',
        databaseURL: 'https://node-test-264012.firebaseio.com',
        projectId: 'node-test-264012',
        storageBucket: 'node-test-264012.appspot.com',
        messagingSenderId: '99739609112',
        appId: '1:99739609112:web:cf2077ebeb790d3be7bb6d'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
