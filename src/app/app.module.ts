import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './components/signup/signup.component';
import { SigninComponent } from './components/signin/signin.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './shared/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { UsersService } from "./shared/services/users.service";
import { AuthService } from "./shared/services/auth.service";

@NgModule( {
    declarations: [
        AppComponent,
        SignupComponent,
        SigninComponent,
        HeaderComponent,
        HomeComponent
    ],
    imports: [
        AngularFireModule.initializeApp( environment.firebase ),
        AngularFirestoreModule,
        AngularFireAuthModule,
        BrowserModule,
        AppRoutingModule,
        NoopAnimationsModule,
        AppMaterialModule,
        ReactiveFormsModule,
        HttpClientModule,
    ],
    providers: [UsersService, AuthService],
    bootstrap: [AppComponent]
} )
export class AppModule {
}
