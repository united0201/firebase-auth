export interface UserRole {
    admin?: boolean;
    user?: boolean;
}

export class User {
    uid: string;
    role: string;
    email: string;
}
