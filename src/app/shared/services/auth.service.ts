import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from '../dto/user.dto';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
import GoogleAuthProvider = firebase.auth.GoogleAuthProvider;

@Injectable( {
    providedIn: 'root'
} )
export class AuthService {
    private accessToken: string;
    private refreshToken: string;
    private username: string;
    private user: User;

    constructor( private afAuth: AngularFireAuth, private afs: AngularFirestore ) {
        const access = window.localStorage.getItem( 'accessToken' );
        const refresh = window.localStorage.getItem( 'refreshToken' );
        if ( access && refresh ) {
            this.accessToken = access;
            this.refreshToken = refresh;
            this.username = window.localStorage.getItem( 'username' );
        }

        this.afAuth.authState.subscribe( value => {
            this.afs.doc( `users/${ value.uid }` ).valueChanges()
                .pipe( map( user => {
                    return user as User;
                } ) )
                .subscribe( user => {
                    this.user = user;
                } );
        } );
    }

    async googleAuth() {
        const provider = new GoogleAuthProvider();
        this.afAuth.auth.signInWithPopup( provider ).then( async res => {
            this.accessToken = await res.user.getIdToken();
            this.username = res.user.email;
            this.refreshToken = await res.user.getIdToken();
            this.addTokens();
        } ).catch(reason => console.log(reason));
    }

    async login( name: string, password: string ): Promise<any> {
        return new Promise<any>( async resolve => {
            try {
                const result = await this.afAuth.auth.signInWithEmailAndPassword( name, password );
                this.accessToken = await result.user.getIdToken();
                this.username = await result.user.email;
                this.refreshToken = await result.user.getIdToken();
                this.addTokens();
                resolve( result );
            } catch ( e ) {
                console.log( 'err', e );
                resolve( e );
            }
        } );
    }

    isAdmin() {
        if ( this.user ) {
            return this.user.role === 'admin';
        } else {
            return false;
        }
    }

    async register( name: string, email: string, password: string ): Promise<any> {
        return new Promise<any>( async resolve => {
            try {
                const result = await this.afAuth.auth.createUserWithEmailAndPassword( email, password );
                this.accessToken = await result.user.getIdToken();
                this.username = await result.user.email;
                this.refreshToken = await result.user.getIdToken();
                resolve( result );
            } catch ( e ) {
                resolve( e );
                console.log( e );
            }
        } );

    }

    logout(): void {
        delete this.refreshToken;
        delete this.accessToken;
        delete this.user;
        this.removeTokens();
    }

    isAuth() {
        return this.accessToken && this.refreshToken;
    }

    getUsername(): string {
        return this.username;
    }

    private removeTokens() {
        window.localStorage.removeItem( 'accessToken' );
        window.localStorage.removeItem( 'refreshToken' );
        window.localStorage.removeItem( 'username' );
    }

    private addTokens() {
        window.localStorage.setItem( 'accessToken', this.accessToken );
        window.localStorage.setItem( 'refreshToken', this.refreshToken );
        window.localStorage.setItem( 'username', this.username );
    }

}
