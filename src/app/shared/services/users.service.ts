import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '../dto/user.dto';

@Injectable( {
    providedIn: 'root'
} )
export class UsersService {
    constructor( private afs: AngularFirestore ) {

    }

    addUser( email: string, role: string, uid: string ) {
        const user: User = {email, role, uid};
        this.afs.collection( `users` ).doc( uid ).set( user );
    }

}
