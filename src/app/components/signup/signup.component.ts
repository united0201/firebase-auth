import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';
import { Router } from '@angular/router';
import { UsersService } from '../../shared/services/users.service';

@Component( {
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.sass']
} )
export class SignupComponent implements OnInit {
    signupForm: FormGroup;
    isError = false;
    errorMessage: string;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router,
        private userService: UsersService ) {
    }

    ngOnInit() {
        this.signupForm = this.formBuilder.group( {
            name: new FormControl( '', Validators.required ),
            email: new FormControl( '', [Validators.required, Validators.email] ),
            password: new FormControl( '', Validators.required ),
        } );
    }

    private getFormData(): any {
        return this.signupForm.value;
    }

    async register() {
        const {name, email, password} = this.getFormData();
        const result = await this.authService.register( name, email, password );
        if ( result.message && result.code ) {
            this.isError = true;
            this.errorMessage = result.message;
        } else {
            console.log(result);
            this.isError = false;
            this.router.navigate( ['/'] );
            this.userService.addUser( email, 'user', result.user.uid );
        }
    }
}
