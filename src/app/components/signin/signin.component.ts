import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component( {
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.sass']
} )
export class SigninComponent implements OnInit {
    signinForm: FormGroup;
    isError = false;
    errorMessage: string;

    constructor( private formBuilder: FormBuilder, private authService: AuthService, private router: Router ) {
    }

    ngOnInit() {
        this.signinForm = this.formBuilder.group( {
            name: new FormControl( '', Validators.required ),
            password: new FormControl( '', Validators.required ),
        } );
    }

    private getFormData(): any {
        return this.signinForm.value;
    }

    async login() {
        const {name, password} = this.getFormData();
        const result = await this.authService.login( name, password );
        if ( result.message && result.code ) {
            this.isError = true;
            this.errorMessage = result.message;
        } else {
            this.isError = false;
            this.router.navigate( ['/'] );
        }
    }
}
